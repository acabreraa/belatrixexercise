package com.belatrix.test;

import org.apache.logging.log4j.Level;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.belatrix.configuration.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
public class TestLog {
	
	@Autowired
	com.belatrix.configuration.Log4j2Configuration logger;
	
	@Test
	public void test() {
		logger.log(TestLog.class, "hola mundo", Level.WARN, true,false,true);
		logger.log(TestLog.class, "hola mundo 2", Level.WARN, false,true,false);
	}

}
