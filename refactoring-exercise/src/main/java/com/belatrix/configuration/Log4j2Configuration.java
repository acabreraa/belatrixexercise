package com.belatrix.configuration;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.appender.db.jdbc.ColumnConfig;
import org.apache.logging.log4j.core.appender.db.jdbc.ConnectionSource;
import org.apache.logging.log4j.core.appender.db.jdbc.JdbcAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.filter.ThresholdFilter;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.belatrix.util.Constantes;

@Component
public class Log4j2Configuration {
	@Autowired
    private Environment env;
	@Autowired
	DataSource logDataSource;
	
	/**
	 * Creates the JDBC appender.
	 *
	 * @param level the level
	 * @return the jdbc appender
	 */
	protected JdbcAppender createJDBCAppender(Level level) {
		ColumnConfig[] columnConfigs = new ColumnConfig[5];
	    columnConfigs[0] =  ColumnConfig.createColumnConfig(null, "APPLICATION", "ACCESS", null, null, Constantes.STR_FALSE, null);
	    columnConfigs[1] =  ColumnConfig.createColumnConfig(null, "ENTRY_DATE", null, null, Constantes.STR_TRUE, null, null);
	    columnConfigs[2] =  ColumnConfig.createColumnConfig(null, "LOGGER", "%logger", null, null, Constantes.STR_FALSE, null);
	    columnConfigs[3] =  ColumnConfig.createColumnConfig(null, "LOG_LEVEL", "%level", null, null, Constantes.STR_FALSE, null);
	    columnConfigs[4] =  ColumnConfig.createColumnConfig(null, "MESSAGE", Constantes.PATTERN_LAYOUT, null, null, Constantes.STR_FALSE, null);
	    ThresholdFilter filter = ThresholdFilter.createFilter(level, null, null);
	    final DataSource datasource =logDataSource;
	    ConnectionSource connection = new ConnectionSource() {			
			public Connection getConnection() throws SQLException {
				return datasource.getConnection();
			}
		};
		return JdbcAppender.createAppender(env.getProperty(Constantes.PROPERTIES_APPENDER_JDBC_NAME), Constantes.STR_TRUE, filter,
				connection, env.getProperty("spring.log4j2.jdbc.bufferSize"),
				env.getProperty("spring.log4j2.jdbc.table"), columnConfigs);
	}
	
	/**
	 * Creates the file appender.
	 *
	 * @param level the level
	 * @return the file appender
	 */
	protected FileAppender createFileAppender(Level level) {
		ThresholdFilter filter = ThresholdFilter.createFilter(level, null, null);
		LoggerContext context = (LoggerContext) LogManager.getContext(false);
		Configuration configuration = context.getConfiguration();
		return FileAppender.createAppender(env.getProperty("spring.log4j2.file.fileName"), Constantes.STR_FALSE,
				Constantes.STR_FALSE, env.getProperty(Constantes.PROPERTIES_APPENDER_FILE_NAME), Constantes.STR_TRUE,
				Constantes.STR_TRUE, Constantes.STR_TRUE, env.getProperty("spring.log4j2.file.bufferSize"),
				getPatternLayout(), filter, Constantes.STR_FALSE, Constantes.STR_EMPTY, configuration);
	}
	
	/**
	 * Creates the console appender.
	 *
	 * @param level the level
	 * @return the console appender
	 */
	protected ConsoleAppender createConsoleAppender(Level level) {
		ThresholdFilter filter = ThresholdFilter.createFilter(level, null, null);
		return ConsoleAppender.createAppender(getPatternLayout(), filter, null,
				env.getProperty(Constantes.PROPERTIES_APPENDER_CONSOLE_NAME), false, false, false);
	}
	
	/**
	 * Log.
	 *
	 * @param clazz the clazz
	 * @param message the message
	 * @param level the level
	 * @param isDataBaseLog the is data base log
	 * @param isFileLog the is file log
	 * @param isConsoleLog the is console log
	 */
	public void log(Class clazz, String message, Level level, boolean isDataBaseLog, boolean isFileLog, boolean isConsoleLog) {
		LoggerContext lc = (LoggerContext) LogManager.getContext(false);
		removeAppenders(lc);
		
		if(isDataBaseLog) {
			lc.getConfiguration().addAppender(lc.getConfiguration().getAppender(env.getProperty(Constantes.PROPERTIES_APPENDER_JDBC_NAME)));
			lc.getRootLogger().addAppender(lc.getConfiguration().getAppender(env.getProperty(Constantes.PROPERTIES_APPENDER_JDBC_NAME)));
		}
		if(isFileLog) {
			lc.getConfiguration().addAppender(lc.getConfiguration().getAppender(env.getProperty(Constantes.PROPERTIES_APPENDER_FILE_NAME)));
			lc.getRootLogger().addAppender(lc.getConfiguration().getAppender(env.getProperty(Constantes.PROPERTIES_APPENDER_FILE_NAME)));
		}
		if(isConsoleLog) {
			ConsoleAppender appender = lc.getConfiguration().getAppender(env.getProperty(Constantes.PROPERTIES_APPENDER_CONSOLE_NAME));
			lc.getConfiguration().addAppender(appender);
			lc.getRootLogger().addAppender(appender);
		}
		lc.updateLoggers();	
	    Logger logger = LogManager.getLogger(clazz.getName());
		logger.log(level, message);
		removeAppenders(lc);
	}
	
	/**
	 * Removes the appenders.
	 *
	 * @param lc the lc
	 */
	public void removeAppenders(LoggerContext lc) {
		lc.getConfiguration().getRootLogger().removeAppender(env.getProperty(Constantes.PROPERTIES_APPENDER_JDBC_NAME));
		lc.getConfiguration().getRootLogger().removeAppender(env.getProperty(Constantes.PROPERTIES_APPENDER_FILE_NAME));
		lc.getConfiguration().getRootLogger().removeAppender(env.getProperty(Constantes.PROPERTIES_APPENDER_CONSOLE_NAME));
		lc.updateLoggers();
	}
	
	PatternLayout getPatternLayout() {
		return PatternLayout.createLayout(
			      Constantes.PATTERN_LAYOUT,
			      null, null, null, Charset.defaultCharset(), true, false, Constantes.STR_EMPTY, Constantes.STR_EMPTY);
	}
}
