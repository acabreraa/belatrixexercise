package com.belatrix.configuration;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.appender.db.jdbc.JdbcAppender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


@SpringBootConfiguration
@ComponentScan({ "com.belatrix"})
public class Application {
	
	@Autowired
    private  Environment env;
	
	@Autowired
	Log4j2Configuration log4j2Configuration;
	
	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
	
	@Primary
    @Bean(name="logDataSource")
    public DataSource logDataSource() {
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring.datasource.url"));
        dataSource.setUsername(env.getProperty("spring.datasource.username"));
        dataSource.setPassword(env.getProperty("spring.datasource.password"));
        return dataSource;
    }
	
	@PostConstruct
	public void enableLog4j2Appenders()  {
	    JdbcAppender jdbcAppender = log4j2Configuration.createJDBCAppender(Level.WARN);
	    jdbcAppender.start();
    
	    FileAppender fileAppender = log4j2Configuration.createFileAppender(Level.WARN);
		fileAppender.start();
    
	    ConsoleAppender consoleAppender = log4j2Configuration.createConsoleAppender(Level.WARN);
	    consoleAppender.start();
	    
	    ((Logger) LogManager.getRootLogger()).addAppender(jdbcAppender);
	    ((Logger) LogManager.getRootLogger()).addAppender(consoleAppender);
	    ((Logger) LogManager.getRootLogger()).addAppender(fileAppender);
	}
	
	
	
	
}
