package com.belatrix.util;

public class Constantes {
	public static final String PROPERTIES_APPENDER_JDBC_NAME = "spring.log4j2.jdbc.appenderName";
	public static final String PROPERTIES_APPENDER_FILE_NAME = "spring.log4j2.file.appenderName";
	public static final String PROPERTIES_APPENDER_CONSOLE_NAME = "spring.log4j2.console.appenderName";
	public static final String PATTERN_LAYOUT = "[%-5level] %d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %c{1} - %msg%n";
	public static final String STR_FALSE = "false";
	public static final String STR_TRUE = "true";
	public static final String STR_EMPTY = "";
}
