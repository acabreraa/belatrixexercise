#mysql commands

create database log_register;

CREATE TABLE LOGS(
    APPLICATION VARCHAR(100) ,
    ENTRY_DATE TIMESTAMP,
    LOGGER VARCHAR(100),
    LOG_LEVEL VARCHAR(100),
    MESSAGE TEXT,
    EXCEPTION TEXT
);

#create folder for file logs and update properties
spring.log4j2.file.fileName=D:\\applogs\\fileLogs.log

#1.	If you were to review the following code, what feedback would you give? Please be specific and indicate any errors that would occur as well as other best practices and code refactoring that should be done. 

#el código usa tecnologia desfasada con la actual, expone los datos de base de datos cada vez q se utiliza el log
#el código no está refactorizado, la funcionalidad no puede ser reutilizable ya que todo está embebido en un solo gran método 
#el código implica complejidad innecesaria y dificil de mantener habiendo nuevas tecnologías que pueden ser implementadas

#mi feedback como se aprecia en mi implementación utiliza una libreria de log log4j2 la cual es una mejora apartir de log4j el cual permite realizar implementacion de log con diferentes fuentes
#se ha separado cada appender por fuente para asi sea facil de utilizar segun se requiera su reutilización e implementacion en otros proyectos
#se ha optado por una configuracion programatica con spring boot y un cambio de fuente log en tiempo de ejecución